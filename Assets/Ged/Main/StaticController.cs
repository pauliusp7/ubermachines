﻿using UnityEngine;
using System.Collections;

public class StaticController : MonoBehaviour {
	GameObject parent;
	// Use this for initialization
	void Start () {
		parent = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	void OnCollisionEnter (Collision other) {
		/*TODO:
		 * Check for player collider
		 */
		Rigidbody[] children = parent.transform.GetComponentsInChildren<Rigidbody> ();
		foreach (var child in children) {
			Debug.LogWarning("veikia");
			if (child.isKinematic == false) break;
			child.isKinematic = false;
			Debug.LogWarning("veikia");
		}
	}
}
