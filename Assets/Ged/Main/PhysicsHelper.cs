﻿using UnityEngine;
using System.Collections;

public class PhysicsHelper : MonoBehaviour {
	public double weight;
	// Use this for initialization
	void Start () {
		weight = 0;
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void CalculateWeight() {
		Rigidbody[] children = this.transform.GetComponentsInChildren<Rigidbody> ();
		foreach (var child in children) {
			weight += child.mass;
		}
	}

}
