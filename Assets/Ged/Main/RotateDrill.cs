﻿using UnityEngine;
using System.Collections;

public class RotateDrill : MonoBehaviour {
	public bool active;
	public float speed;
	private float rot;
	// Use this for initialization
	void Start () {
		if (speed == 0)
			speed = 1;
	}
	
	// Update is called once per frame
	void Update () {
	if (active) {
			transform.Rotate(Vector3.back * Time.deltaTime * speed);
		}
	}
}
