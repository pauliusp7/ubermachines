﻿using UnityEngine;
using System.Collections;

public class MechBotController : MonoBehaviour {


	public GameObject destructionFX;
	public GameObject postFX;
	
	public Transform target;

	public Transform Target {
		get {
			try{
				target = GameObject.Find("ParentusSupremus").transform.FindChild("Cube").transform;
				if( target != null){
					return target;
				}
				else{
					return transform;	
				}
			}
			catch(System.Exception ex){
				return transform;
			}
		}
	}

	NavMeshAgent agent;
	//
	
	int hp = 1;
	//
	public bool fire;
	
	Animation animation;
	
	
	Vector3 lastFramePosition;
	
	RWSP_RocketTurret turretL;
	RWSP_RocketTurret turretR;
	
	void Start () {

		
		if(target == null){
			
			target = GameObject.Find("ParentusSupremus").transform;
		}

		//set uo turret things
		
		animation = GetComponent<Animation>();
		agent = GetComponent<NavMeshAgent>();
		
		turretL = transform.FindChild("RocketLauncher1TubeL").GetComponent<RWSP_RocketTurret>();
		turretR = transform.FindChild("RocketLauncher1TubeR").GetComponent<RWSP_RocketTurret>();
		
		turretL.Target = Target;
		turretR.Target = Target;
		

		Activate();
	}
	
	
	public void Activate(){
	
	
		StartCoroutine( Spawn());
		
	}
	
	IEnumerator Spawn(){
		
		agent.enabled = false;
		animation.Play("jumpSmashAttack");
		
		while( animation.isPlaying && !animation.IsPlaying("idle")){
			yield return null;
		}

		
		lastFramePosition = transform.position;
		agent.enabled = true;
	}
	
	void Fire(){
	
		turretL.FiringVolley = true;
		turretR.FiringVolley = true;
	}
	
	// Update is called once per frame
	
	float fireCooldown = 7f;
	
	void Update () {
	
	
		fireCooldown -= Time.deltaTime;
		if( fireCooldown <= 0){
			fireCooldown = Random.Range(11.5f, 60.5f);
			fire = true;
		}
		
	
		if(fire){
			Fire();
			fire = false;
		}

		
		
		// 
		Vector3 currentFramePosition  = transform.position;
		float distance  = Vector3.Distance(lastFramePosition, currentFramePosition);
		
		lastFramePosition = currentFramePosition;
		float currentSpeed = Mathf.Abs(distance)/Time.deltaTime;
		
		//
	
		if (currentSpeed > 0.1)
			animation.CrossFade("walkForward");
		else
			animation.CrossFade("idle");
	
		if(agent.isActiveAndEnabled){
			GetComponent<NavMeshAgent>().SetDestination(Target.position);
		}
	}
	
	public void GetHit(int dmg=1){
	
		hp -= dmg;
		if(hp <=0){
			StartCoroutine( Destruct() );
		}
	}
	
	IEnumerator Destruct(){
	
		yield return null;

		
		GameObject.Instantiate(destructionFX, transform.position, Quaternion.identity);
		
		Destroy(gameObject);
	}
}
