﻿using UnityEngine;
using System.Collections;

public class RWSP_ExplosionLevelModifier : MonoBehaviour {

	public ParticleSystem MainExploParticleSystem;
	public ParticleSystem MainSmokeParticleSystem;
	public RocketTypes RocketExplodingType = RocketTypes.RocketType1;
	private float rocketLevelNum = 0;

	// Use this for initialization
	void Start () {

	}

	public void SetRocketType(RocketTypes rocketTypeIn) {
		RocketExplodingType = rocketTypeIn;

		int rocketLevelNumInt = (int)RocketExplodingType + 1;
		rocketLevelNum = (float)rocketLevelNumInt / 2;

		// Set Particle System Variables Based On Level
		if (MainExploParticleSystem != null) {
			MainExploParticleSystem.startSize = Random.Range(rocketLevelNum * 5.0f, rocketLevelNum * 7.5f);
		}
		if (MainSmokeParticleSystem != null) {
			MainSmokeParticleSystem.startSize = Random.Range(rocketLevelNum * 5.0f, rocketLevelNum * 7.5f);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
