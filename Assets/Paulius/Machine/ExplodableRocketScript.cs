﻿using UnityEngine;
using System.Collections;

public class ExplodableRocketScript : MonoBehaviour {

	void OnTriggerEnter(Collider col) {				
	
		//Check if I hit a Mech or a Player
		
		Debug.Log(col.gameObject.name);
		
		var player = col.gameObject.GetComponentInParent<ParentController>();
		
		if(player != null){
		
			
		}
		
		else{
		
			var mech = col.gameObject.GetComponentInParent<MechBotController>();
			
			if (mech != null){
				
				Debug.Log("hit" + mech.GetType().ToString());
				mech.GetHit();
			}
		}
	}
}
