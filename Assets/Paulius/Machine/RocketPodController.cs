﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class RocketPodController : MonoBehaviour {
		
	public enum FireType { Auto, Manual};
	
	public KeyCode key;	
	public float fireDelay = 3.3f;
	public FireType fireType;
	
	RWSP_RocketTurret turret;
	public float cooldown;

	//
	private Transform dummyTarget;

	public Transform target {
		get {
			return turret.Target;
		}
		set {
			turret.Target = value;
		}
	}		
		
		
	// Use this for initialization
	void Start () {
		cooldown = fireDelay;
		turret = transform.FindChild("RocketLauncher9Tube").GetComponent<RWSP_RocketTurret>();
	}
	
	
	
	// Update is called once per frame
	void Update () {
			
		cooldown -= Time.deltaTime;	
		
		if( cooldown <= 0f){
		
			target = GetBestTarget();
		
			if(fireType == FireType.Manual & Input.GetKey(key)){
				
				Debug.Log("fire!");
				turret.FiringVolley = true;
				cooldown = fireDelay;
			}
			else if(fireType == FireType.Auto ){
				
				turret.FiringVolley = true;
				cooldown = fireDelay;
			}
			
		}
	}
	
	Transform GetBestTarget(){
	
		var targets = GameObject.FindGameObjectsWithTag("EvilMech");
		
		if(targets.Length == 0){
		
			if(dummyTarget == null){
				//create a dummy gObj in front
				var dummy = new GameObject();
				dummyTarget = dummy.transform;
				dummy.name = "DummyTarget";
				dummy.transform.SetParent(turret.transform);
				
				dummy.transform.localPosition = new Vector3(-5000f, 0, 0) ;
				
				dummy.transform.SetParent(transform);
				
				return dummy.transform;
			}
			else{
				return dummyTarget;
			}
		}
		else if(targets.Length == 1){
			return targets[0].transform;
		}
		else{
			var distToTargets = new Dictionary<float, Transform>(); 
			foreach(var target in targets){
			
				var dist = Vector3.Distance(transform.position, target.transform.position);
				distToTargets.Add(dist, target.transform);
			}
			return distToTargets.OrderBy(l => l.Key).First().Value.transform;
		}
	
	}
}
