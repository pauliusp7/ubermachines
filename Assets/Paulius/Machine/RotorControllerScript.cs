﻿using UnityEngine;
using System.Collections;

public class RotorControllerScript : MonoBehaviour {

	public char UpKey = 'u';
	public char DownKey = 'd';


	public bool isActive;
	
	// Update is called once per frame
	public float maxVelocity ;
	public float acceleration;
	
	
	Rigidbody rb;
	
	
	void Start(){
	
		rb = GetComponent<Rigidbody>();
		acceleration = Physics.gravity.y*-1 + rb.mass * acceleration;
	}
	
	void Update () {
	
		if( isActive){
			if(Input.GetKey(KeyCode.U)){
				Debug.Log(((KeyCode)UpKey).ToString());
				if( rb.velocity.y <= maxVelocity){
					
					rb.AddRelativeForce(new Vector3(0f, acceleration, 0f));
				}
			}
			if (Input.GetKey(KeyCode.LeftArrow)){
				rb.AddRelativeForce(new Vector3(-2f, 0, 0f));
			}
			else if (Input.GetKey(KeyCode.RightArrow)){
				rb.AddRelativeForce(new Vector3(2f, 0, 0f));
			}
			if(Input.GetKey(KeyCode.UpArrow)){
				rb.AddRelativeForce(new Vector3(3f, 0, 0f));
			}
			
			else if(Input.GetKeyDown(DownKey.ToString())){
			
		
			}
			
		}
	}
}
