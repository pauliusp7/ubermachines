﻿using UnityEngine;
using System.Collections;

public abstract class GenericTrapComponent : MonoBehaviour {

	public abstract void StartTrap();
	public abstract void StopTrap();
	public abstract void ResetTrap();
	
	public bool activated = false;
	protected bool isActive = false;
}
