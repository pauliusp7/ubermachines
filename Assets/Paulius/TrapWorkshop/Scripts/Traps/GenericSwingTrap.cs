﻿using UnityEngine;
using System.Collections;

public class GenericSwingTrap : GenericTrapComponent {
	
	
	//Configurable stuff:
	
	
	
	//internal
	GameObject handle;
	GameObject weapon;
	GameObject trapBase;

	Vector3 weaponInitPosition;

	//states

	bool IsActive {
		get {
			return isActive;
		}
		set {
			handle.GetComponent<Rigidbody>().isKinematic = !value;
			weapon.GetComponent<Rigidbody>().isKinematic = !value;
			isActive = value;
		}
	}

	void Start () {
	
		activated = false;
		handle = transform.Find("Handle").gameObject;
		weapon = transform.Find("Weapon").gameObject;
		trapBase = gameObject;
				
		weaponInitPosition = weapon.transform.position;
		handle.GetComponent<Rigidbody>().isKinematic = true;
		weapon.GetComponent<Rigidbody>().isKinematic = true;		
				
		//init handle joint;
		var handleJoint = handle.GetComponent<HingeJoint>();
		handleJoint.useMotor = false;
		//TODO make this configurable
		var limits = new JointLimits(); 
		limits.max = 90f;
		limits.min = -60f;
		
		

		
		
	}
	
	void Update(){
	
		if(Input.GetKeyDown(KeyCode.X)){
			if( activated){
				
				ResetTrap();
			}
			else{
				StartTrap();
			}
		}
	}
	// Callable Stuff (Public)
	public override void StartTrap(){
	
		activated = true;
		handle.GetComponent<Rigidbody>().isKinematic = false;
		weapon.GetComponent<Rigidbody>().isKinematic = false;
		RunMotor();
	}
	
	public override void StopTrap(){
		StopMotor();
	}
	
	//
	
	public override void ResetTrap(){
		StartCoroutine(RewindTrap());
	}
	
	private IEnumerator RewindTrap(){ //rewinds weapon to init position
	

		Debug.Log("RewindTrap");
		RunMotor(5000f, -22.5f);
		
		while( ! (weapon.transform.position.x > (weaponInitPosition.x - 0.15f) && weapon.transform.position.x < (weaponInitPosition.x + 0.15f))){
			yield return new WaitForEndOfFrame();
			handle.GetComponent<Rigidbody>().useGravity  = false; //HACK
			weapon.GetComponent<Rigidbody>().useGravity  = false; //HACK
		}
		handle.GetComponent<Rigidbody>().useGravity  = true; //HACK
		weapon.GetComponent<Rigidbody>().useGravity  = true; //HACK
		
		StopMotor();
		Start();
	}
	
	// internal crap
	void RunMotor(float force = 100f, float velocity = 10f){
	
	
		var handleJoint = handle.GetComponent<HingeJoint>();
		
		var motor = new JointMotor();
		motor.freeSpin = true;
		motor.force = force;
		motor.targetVelocity = velocity;
		
		handleJoint.motor = motor;
		handleJoint.useMotor = true;
	}
	
	void StopMotor(){
		 Debug.Log("StopMotor");
		 var handleJoint = handle.GetComponent<HingeJoint>();
		 handleJoint.useMotor = false;
	}


}
