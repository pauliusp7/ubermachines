﻿using UnityEngine;
using System.Collections;

public class GenericWreckingBallTrap : GenericTrapComponent {
	
	
	//Configurable stuff:
	
	
	
	//internal
	GameObject handle;
	GameObject weapon;
	GameObject trapBase;

	Vector3 weaponInitPosition;

	//states
	
	
	bool IsActive {
		get {
			return isActive;
		}
		set {
			isActive = value;
		}
	}

	void Start () {
		trapBase = transform.Find("Base").gameObject;
		activated = false;

	}
	// Callable Stuff (Public)
	public override void StartTrap(){
		activated = true;
	}
	
	public override void StopTrap(){
		activated = false;
	}
	
	//
	
	public override void ResetTrap(){
		StopTrap();
	}
	

	
	// internal crap

	void OnMouseDown() {
		if( activated){
			
			ResetTrap();
		}
		else{
			StartTrap();
		}
	}
	
	public bool positiveDir = true;
	
	void Update () {
	
		if(trapBase.transform.rotation.z > 35f){
			positiveDir = false;
		}
		else if(trapBase.transform.rotation.z < -35f){
			positiveDir = true;
		}
	
		if(activated){
		
			if(positiveDir){
				trapBase.transform.RotateAround(transform.position, new Vector3(0,0,1f), 0.35f);
			}
			else{
				trapBase.transform.RotateAround(transform.position, new Vector3(0,0,1f), -0.35f);
			}
		}
	}
}
