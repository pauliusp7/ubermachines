﻿using UnityEngine;
using System.Collections;

public class GenericExplodableTHing : MonoBehaviour {

	public GameObject ExplosionFX;
	public GameObject SpawnFX;

	void Start(){
	
	//	var trail = GameObject.Instantiate(SpawnFX);
	//	trail.transform.parent = this.transform;
	}

	void OnCollisionEnter(Collision collision)  { //Smash all stuff
		
		CreateExplosion();
	}
	
	
	protected void CreateExplosion(float radius=.5f, float force =3.75f){
		
		
		var mask = new LayerMask();
		mask.value = LayerMask.GetMask( new string[]{"Explodable"} );
		var ExplosionZone = Physics.SphereCastAll(transform.position,radius, new Vector3(1,1,1), radius,mask );
		
		Debug.Log("ExplosionZone: "+ExplosionZone.Length);
		
		foreach(var body in ExplosionZone){
			
			var rb = body.transform.gameObject.GetComponent<Rigidbody>();
			if( rb != null){
				rb.AddExplosionForce(force, transform.position, radius, 3f, ForceMode.Impulse);
				
				//FIXME HACK for debug
				body.transform.GetComponent<MeshRenderer>().material.color = Color.red;
			}
		}
		StartCoroutine( DestroyAfterSmth());

	}
	
	IEnumerator DestroyAfterSmth(){
	
		GameObject.Instantiate(ExplosionFX, transform.position, Quaternion.identity);
		yield return new WaitForSeconds(0.1f);
		Debug.Log("D");
		//Destroy(gObj);
		Destroy(gameObject);
	}
}
