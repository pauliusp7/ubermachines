﻿using UnityEngine;
using System.Collections;

public abstract class GenericWeaponComponent : MonoBehaviour {
	
	protected int weaponUses = 1;

	public GenericTrapComponent Trap {
		get {
			return transform.parent.GetComponent<GenericTrapComponent>();
		}
	}

	public abstract void ActivateWeapon();

	void OnCollisionEnter(Collision collision)  { //Smash all stuff
	
		ActivateWeapon();
	}
	
	protected void CreateExplosion(float radius=10f, float force =10f){
	
	
		var mask = new LayerMask();
		mask.value = LayerMask.GetMask( new string[]{"Explodable"} );
		var ExplosionZone = Physics.SphereCastAll(transform.position,radius, new Vector3(1,1,1), radius,mask );
		
		Debug.Log("ExplosionZone: "+ExplosionZone.Length);
		
		foreach(var body in ExplosionZone){
		
			var rb = body.transform.gameObject.GetComponent<Rigidbody>();
			if( rb != null){
				rb.AddExplosionForce(force, transform.position, radius, 3f, ForceMode.Impulse);
			
				//FIXME HACK for debug
				body.transform.GetComponent<MeshRenderer>().material.color = Color.red;
			}
		}
		
		
	}
	

}
