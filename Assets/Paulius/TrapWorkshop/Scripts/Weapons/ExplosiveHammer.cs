﻿using UnityEngine;
using System.Collections;

public class ExplosiveHammer : GenericWeaponComponent {

	public override void ActivateWeapon(){
		
		
		if( weaponUses > 0){
			Debug.Log("ActivateWeapon");
			weaponUses--;
			CreateExplosion();
			Trap.StopTrap();
		}
	}
	
	//HACK for now to show feacure
	void OnMouseDown() {
		if( Trap.activated){
		
			Trap.ResetTrap();
		}
		else{
			Trap.StartTrap();
		}
	}
	
}
