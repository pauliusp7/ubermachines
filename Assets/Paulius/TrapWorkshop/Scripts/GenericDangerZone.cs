﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class GenericDangerZone : MonoBehaviour {

	public bool onlyActiveWhileTargetInZone;
	
	public bool isActive;

	public string targetTag = "Player";

	//
	public int projectileSpawnFrequency = 30; //Essiantially how many objects per min
	public float spawnHeight = 20f;
	public float spawnDownardForce = 20f;
	public float spawnForceDeviation = 5.5f;
	
	//
	public List<GameObject> projectiles = new List<GameObject>();

	List<GameObject> activeProjectiles = new List<GameObject>();
	//

	void Start(){
		
		try{
			foreach(var obj in projectiles){
				obj.GetComponent<Rigidbody>();
			}
		}
		catch(Exception ex){
			Debug.LogError("Wtf?? Wy so rytard, wy not add rb to projectile??? ah??");
		}
	
		StartCoroutine(ProjectileSpawner());
	}
	
	IEnumerator ProjectileSpawner(){
	
		float toSpawn =0f;
	
		while(true){
			if(isActive){
				
				//Spawn prpjectiles in random points in danger zone:
				var bounds = GetComponent<BoxCollider>().bounds;
			
				var spawnCount = 60f/projectileSpawnFrequency;
			
				toSpawn+= spawnCount;
				
				if( (int)toSpawn > 1){
					
					for(var it=0; it <(int)toSpawn; it++){
					
						var rnd = new System.Random();
					
						var spawnPoint = new Vector3(UnityEngine.Random.Range(bounds.min.x, bounds.max.x), spawnHeight, UnityEngine.Random.Range(bounds.min.z, bounds.max.z));  
						var projectilePrefab = projectiles[UnityEngine.Random.Range(0, projectiles.Count-1)];
						
						var projectile = GameObject.Instantiate( projectilePrefab, spawnPoint, Quaternion.identity) as GameObject;
						
						//
						
						var spawnForce  = new Vector3(UnityEngine.Random.Range(spawnForceDeviation*-1f, spawnForceDeviation),spawnDownardForce*-1,UnityEngine.Random.Range(spawnForceDeviation*-1f, spawnForceDeviation) );
						projectile.GetComponent<Rigidbody>().velocity = spawnForce;//.AddForce( spawnForce, ForceMode.Impulse);
						
						toSpawn--;
					}
				
				}
			}
							
			yield return new WaitForSeconds(1f);
		}
	}
	
	
	void OnTriggerEnter(Collider other) {
		
		Debug.Log("Enter Danger Zone");
		
		if(other.transform.tag =="Player"   && onlyActiveWhileTargetInZone){
			Debug.Log("Activate Danger Zone");
			isActive = true;
		}
	}
	
	void OnTriggerExit(Collider other) {
		
		Debug.Log("Exit Danger Zone");
		
		if(other.transform.tag =="Player"  && onlyActiveWhileTargetInZone){
			Debug.Log("Deactivate Danger Zone");
			isActive = false;
		}
	}
}
