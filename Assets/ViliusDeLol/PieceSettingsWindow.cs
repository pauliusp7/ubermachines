﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PieceSettingsWindow : MonoBehaviour {
	public Text infoText;
	public Text forwardKey;
	public Text backwardKey;
	private PieceInfo piece;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void Init(PieceInfo piece) {
		this.piece = piece;
		infoText.text = "Settings for " + piece.gameObject.name;
		forwardKey.text = piece.forwardKey.ToString();
		backwardKey.text = piece.backwardKey.ToString();
	}

	public void RebindForward() {
			
		StartCoroutine (WaitForInput (piece, true));
	}

	public void RebindBackward() {
		StartCoroutine (WaitForInput (piece, false));	

	}

	IEnumerator WaitForInput(PieceInfo target, bool forward) {

		while (Application.isPlaying) {
			if(Input.anyKey) {
				foreach(KeyCode vKey in System.Enum.GetValues(typeof(KeyCode))){
					if(Input.GetKey(vKey)){
						if(forward)
							target.forwardKey = vKey;
						else
							target.backwardKey = vKey;

					}
				}
				break;
			}

			yield return null;
		}

		forwardKey.text = piece.forwardKey.ToString();
		backwardKey.text = piece.backwardKey.ToString();
	}

	public void Close() {
		BuildController.action = BuildingAction.Config;
		Destroy (gameObject);
	}
}
