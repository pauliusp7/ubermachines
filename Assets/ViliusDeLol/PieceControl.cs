﻿using UnityEngine;
using System.Collections;

public class PieceControl : MonoBehaviour {
	private PieceInfo info;
	private HingeJoint hinge;
	private HingeJoint Hinge {
		get{
			if(hinge==null)
				hinge = GetComponent<HingeJoint>();
			return hinge;
		}
		set {
			hinge = value;
		}
	}

	private Rigidbody rigidbody;
	private Rigidbody Rigidbody {
		get{
			if(rigidbody==null)
				rigidbody = GetComponent<Rigidbody>();
			return rigidbody;
		}
		set {
			rigidbody = value;
		}
	}
	// Use this for initialization
	void Start () {
		info = GetComponent<PieceInfo> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	
	
		if(info.pieceType == PieceType.SteeringThingy){
		
		//hacky #HACK  TROLOLO
		if(gameObject.GetComponent<FixedJoint>() != null){
		
				GetComponent<Rigidbody>().mass = 10f;
		
				var rb = gameObject.GetComponent<FixedJoint>().connectedBody;
				var bf = gameObject.GetComponent<FixedJoint>().breakForce * 4;
				
				HingeJoint j = gameObject.AddComponent<HingeJoint> ();
				j.connectedBody = rb;
					
				float sign = Mathf.Sign(transform.eulerAngles.y/90 - 2);
				j.axis = new Vector3(0,sign,0);
				j.useMotor = false;
			//	JointMotor motor = new JointMotor ();
			//	motor.force = 750;
			//	j.breakForce = bf;
			//	j.motor = motor;
			}

		
		
		//
		

			if(Hinge!=null) {
				if (Input.anyKey) {
					if (Input.GetKey (info.leftKey)) {
						Debug.Log("SteeringThingy");
						GetComponent<Rigidbody>().AddRelativeTorque( new Vector3(0f,555f,0f));

					} else if (Input.GetKey (info.rightKey)) {
						GetComponent<Rigidbody>().AddRelativeTorque( new Vector3(0f,-555f,0f));
					}
				} 
			}
			
		}
	
		else if (info.pieceType == PieceType.Spinning) {
			if(Hinge!=null) {
			JointMotor motor = Hinge.motor;
			if (Input.anyKey) {
				if (Input.GetKey (info.forwardKey)) {
					motor.targetVelocity = info.strength;
					Hinge.motor = motor;
				} else if (Input.GetKey (info.backwardKey)) {
					motor.targetVelocity = -info.strength;
					Hinge.motor = motor;
				}
		
			} else {
				motor.targetVelocity = 0;
				Hinge.motor = motor;
			}

			
		}
		} else if (info.pieceType == PieceType.Flying) {
			if (Input.GetKey (info.forwardKey)) {
				Vector3 rotorRotation = transform.eulerAngles.normalized;
				Rigidbody.AddRelativeForce(new Vector3(0,info.strength,0));
			} else if (Input.GetKey (info.backwardKey)) {
				Rigidbody.AddRelativeForce(new Vector3(0,-info.strength,0));
			}
		}
		

}
} 
