﻿using UnityEngine;
using System.Collections;

public enum PieceType{Static,Spinning, Flying, SteeringThingy};

public class PieceInfo : MonoBehaviour {
	public PieceType pieceType;
	public float strength;
	public KeyCode forwardKey;
	public KeyCode backwardKey;
	
	public KeyCode leftKey = KeyCode.A;
	public KeyCode rightKey = KeyCode.D;
	

	// Use this for initialization
	void Start () {
	
		gameObject.layer = 21;
		gameObject.tag = "Player";
	}


	// Update is called once per frame
	void Update () {

	}
}
