﻿using UnityEngine;
using System.Collections;

public enum BuildingAction{Build, Delete, Config, Defocused};

public class BuildController : MonoBehaviour {
	public GameObject [] pieces; 
	public static BuildingAction action;
	public GameObject pieceSettingsWindow;
	// Use this for initialization
	void Start () {
	
	}

	public static BuildController Instance() {
		return GameObject.FindObjectOfType<BuildController> ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void EditSettings(PieceInfo piece) {
		action = BuildingAction.Defocused;
		GameObject temp = Instantiate (pieceSettingsWindow);
		temp.GetComponent<PieceSettingsWindow> ().Init (piece);
	}

	public void SetPiece(int id) {
		PieceSnapping.selectedPiece = pieces [id];
		action = BuildingAction.Build;
	}

	public void SetNull() {
		PieceSnapping.selectedPiece = null;
		action = BuildingAction.Delete;
	}

	public void SetEdit() {
		PieceSnapping.selectedPiece = null;
		action = BuildingAction.Config;
	}
}
