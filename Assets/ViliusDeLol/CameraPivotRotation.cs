﻿using UnityEngine;
using System.Collections;

public class CameraPivotRotation : MonoBehaviour {
	Vector3 target = Vector3.zero;
	private float xRot=0;
	private float yRot=90;
	private Vector2 lastMousePos;
	private Vector3 targetPos;
	private float lerpAmount = 0.15f;
	private float cameraDistance=10f;
	private float targetZoom=10f;
	// Use this for initialization
	void Start () {
		targetPos = new Vector3 (cameraDistance, 0, 0);
	}
	
	// Update is called once per frame
	void Update () {
		transform.position = Vector3.Lerp (transform.position, targetPos, lerpAmount);

		transform.LookAt (target);
		if (Input.GetMouseButton (1)) {
			Rotate (lastMousePos - (Vector2)Input.mousePosition);
		} else {
			Rotate (Vector2.zero);
		}
		lastMousePos = (Vector2)Input.mousePosition;

		UpdateZoom ();

		cameraDistance = Mathf.Lerp (cameraDistance, targetZoom, lerpAmount);
		//Rotate (new Vector2(1,0));
	}

	void UpdateZoom() {
		targetZoom -= Input.mouseScrollDelta.y;
			if(targetZoom> 30)
				targetZoom = 30;
			if(targetZoom<3)
				targetZoom = 3;
	}

	void Rotate(Vector2 deg) {
		xRot += deg.x;
		yRot += deg.y;
		Vector3 endPos = transform.position;
		endPos.x = Mathf.Cos (xRot * Mathf.Deg2Rad) * Mathf.Sin (yRot * Mathf.Deg2Rad) * cameraDistance;
		endPos.z = Mathf.Sin (xRot * Mathf.Deg2Rad) * Mathf.Sin (yRot * Mathf.Deg2Rad) * cameraDistance;
		endPos.y = Mathf.Cos (yRot * Mathf.Deg2Rad) * cameraDistance;
		targetPos = endPos;
	}
}
