﻿using UnityEngine;
using System.Collections;
using System.Linq;



public class PieceSnapping : MonoBehaviour {
	GameObject piecePreview;
	public static GameObject selectedPiece;
	public bool deletable=true;
	private BoxCollider buildingBox;
	private GameObject parent=null;
	private PieceInfo pieceInfo;
	private float breakForce=1100;
	// Use this for initialization
	void Start () {
		pieceInfo = GetComponent<PieceInfo> ();
		if (buildingBox == null) {
			GetComponent<Collider>().enabled = false;
			buildingBox = gameObject.AddComponent<BoxCollider> ();
			buildingBox.size = new Vector3(1/transform.localScale.x, 1/transform.localScale.y, 1/transform.localScale.z);
			buildingBox.center = Vector3.zero;
		}
	}

	public void PrepareForGameplay() {
		GetComponent<Rigidbody> ().isKinematic = false;
		RaycastAndAddJoint ();
		if ((pieceInfo.pieceType == PieceType.Spinning && parent.GetComponent<PieceInfo>().pieceType != PieceType.Spinning) || pieceInfo.pieceType == PieceType.Flying)
			gameObject.AddComponent<PieceControl> ();
		else if ((pieceInfo.pieceType == PieceType.SteeringThingy && parent.GetComponent<PieceInfo>().pieceType != PieceType.Spinning) || pieceInfo.pieceType == PieceType.Flying)
			gameObject.AddComponent<PieceControl> ();
				
		Destroy (buildingBox);
		GetComponent<Collider> ().enabled = true;
		Destroy (this);
	}

	public Rigidbody GetRigidbody(GameObject target) {
		if(target.GetComponent<Rigidbody> () == null) {
			Rigidbody rig = target.gameObject.AddComponent<Rigidbody>();

			//rig.constraints = (RigidbodyConstraints.FreezePositionZ);

		}
		return target.GetComponent<Rigidbody> ();
	}
	
	public void RaycastAndAddJoint() {
		RaycastHit info;


		for (int i=0; i<3; i++) {
			for (int y=-1; y<2; y++) {
				Vector3 dir = Vector3.zero;
				switch (i) {
				case 0:
					dir = Vector3.up;
					break;
				case 1:
					dir = Vector3.left;
					break;
				case 2:
					dir = Vector3.forward;
					break;
				}
				dir*=y;
				if (Physics.Raycast (transform.position, dir, out info, 1f)) {
					if (pieceInfo.pieceType == PieceType.Static) {
						//Static pieces attach fixed joints to other static pieces
						if(info.collider.GetComponent<PieceInfo>().pieceType != PieceType.Spinning) {
						FixedJoint j = gameObject.AddComponent<FixedJoint> ();
						j.connectedBody = GetRigidbody(info.collider.gameObject);
							j.breakForce = breakForce;
						}
					else if (pieceInfo.pieceType == PieceType.SteeringThingy) {
					
						if(info.collider.gameObject == parent && info.collider.gameObject.GetComponent<PieceInfo>().pieceType!=PieceType.Spinning) {
						HingeJoint j = gameObject.AddComponent<HingeJoint> ();
						j.connectedBody = GetRigidbody(info.collider.gameObject);
							
						float sign = Mathf.Sign(transform.eulerAngles.y/90 - 2);
						j.axis = new Vector3(0,sign,0);
					//	j.useMotor = true;
						JointMotor motor = new JointMotor ();
						motor.force = 750;
					//	j.breakForce = breakForce;
					//	j.motor = motor;
						}
						else {
							Joint j = gameObject.AddComponent<FixedJoint> ();
							j.breakForce = breakForce;
							j.connectedBody = GetRigidbody(info.collider.gameObject);
						}
					}
						
					} else {
						//Wheels attach a hinge joint to it's parent piece (if it's not a wheel) and fixed joints to any other pieces
						if(info.collider.gameObject == parent && info.collider.gameObject.GetComponent<PieceInfo>().pieceType!=PieceType.Spinning) {
						HingeJoint j = gameObject.AddComponent<HingeJoint> ();
						j.connectedBody = GetRigidbody(info.collider.gameObject);

						float sign = Mathf.Sign(transform.eulerAngles.y/90 - 2);
						j.axis = new Vector3(0,sign,0);
						j.useMotor = true;
						JointMotor motor = new JointMotor ();
						motor.force = 1000;
							j.breakForce = breakForce;
						j.motor = motor;
						}
						else {
							Joint j = gameObject.AddComponent<FixedJoint> ();
							j.breakForce = breakForce;
							j.connectedBody = GetRigidbody(info.collider.gameObject);
						}
					}
					//j = gameObject.AddComponent<FixedJoint>();
					//j.connectedBody = info.collider.GetComponent<Rigidbody>();
				}
			}
		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnMouseEnter() {
		if (selectedPiece != null) {
			if (piecePreview == null) {
				piecePreview = GameObject.CreatePrimitive (PrimitiveType.Cube);
				
				
				//HACK I BROKE UR SYSTEWM<>>> TROLOLO
				try{
				piecePreview.GetComponent<MeshFilter> ().mesh = selectedPiece.GetComponent<MeshFilter> ().sharedMesh;
				}
				catch(System.Exception ex){
					try{
					piecePreview.GetComponent<MeshFilter> ().mesh = selectedPiece.GetComponentInChildren<MeshFilter> ().sharedMesh;
					}
					catch(System.Exception ex2){
						var obj = selectedPiece.transform.FindChild("Handle");
						Debug.Log(obj);
						obj = GameObject.CreatePrimitive(PrimitiveType.Quad).transform;
						piecePreview.GetComponent<MeshFilter> ().mesh = obj.gameObject.GetComponentInChildren<MeshFilter> ().sharedMesh;
					}
				}

				Material tempMat = piecePreview.GetComponent<MeshRenderer> ().material;
				tempMat.color = new Color (0.25f, 1, 0.25f, 0.5f);


				Destroy (piecePreview.GetComponent<Collider> ());
				Vector3 pieceScale = selectedPiece.transform.localScale;
				piecePreview.transform.localScale = pieceScale;
				piecePreview.transform.parent = transform.parent;
				//piecePreview.transform.localScale = new Vector3(pieceScale.x/parentScale.x, pieceScale.y/parentScale.y,pieceScale.z/parentScale.z);
				piecePreview.transform.rotation = selectedPiece.transform.rotation;

			}
		} else {
			if(BuildController.action == BuildingAction.Delete) {
				if(deletable)
					GetComponent<Renderer>().material.color = Color.red;
			}
			else if(BuildController.action == BuildingAction.Config) {
				if(pieceInfo.pieceType != PieceType.Static) {
					GetComponent<Renderer>().material.color = Color.cyan;
				}
			}
			
			
		}
	}

	void OnMouseOver() {
		if (piecePreview != null) {
			Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit info;
			Physics.Raycast(ray, out info, 100);
			var cols = GetComponents<Collider>();
			Collider enabledColl = cols.First(x => x.enabled==true);
			Vector3 cubePos = enabledColl.ClosestPointOnBounds(info.point);
			cubePos = MaxCoordinateOnly(transform.position  - cubePos);
			cubePos +=transform.position;
			piecePreview.transform.localPosition = cubePos;

			Vector3 lastRot = selectedPiece.transform.eulerAngles;
			piecePreview.transform.LookAt(transform.position);
			piecePreview.transform.eulerAngles += lastRot;




		}
	}

	void OnMouseExit() {
		if (selectedPiece != null) {
			if (piecePreview != null) {
				Destroy (piecePreview);
			}
		} else {
			if(deletable)
			GetComponent<Renderer>().material.color = Color.white;
		}
	}

	void OnMouseDown() {
		if (selectedPiece != null) {
			if (piecePreview != null) {
				GameObject newPiece = Instantiate (selectedPiece);
				newPiece.transform.parent = transform.parent;
				newPiece.transform.position = piecePreview.transform.position;
				newPiece.transform.eulerAngles = piecePreview.transform.eulerAngles;
				newPiece.GetComponent<PieceSnapping> ().parent = gameObject;
			}
		} else {
			if(BuildController.action == BuildingAction.Delete) {
			if(deletable)
				Destroy (gameObject);
			}
			else if(BuildController.action == BuildingAction.Config) {
				if(pieceInfo.pieceType != PieceType.Static) {
					BuildController.Instance().EditSettings(pieceInfo);
				}
			}
		}
	}
	//I don't know why and how this works but it does
	// Maybe use axis aligned bounding box
	private Vector3 MaxCoordinateOnly(Vector3 input) {
		Vector3 result = input;
		if (Mathf.Abs(result.x) > Mathf.Abs(result.y)) {
			if(Mathf.Abs(result.x) > Mathf.Abs(result.z)) {
				result = new Vector3(-Mathf.Sign(result.x),0,0);
			}
			else {
				result = new Vector3(0,0,-Mathf.Sign(result.z));
			}
		} else {
			if(Mathf.Abs(result.y) > Mathf.Abs(result.z)) {
				result = new Vector3(0,-Mathf.Sign(result.y),0);
			}
			else {
				result = new Vector3(0,0,-Mathf.Sign(result.z));
			}
		}
		return result;
	}
}
