﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ParentController : MonoBehaviour {
	
	bool loadMainLevel;

	Transform mainCube;

	// Use this for initialization
	void Start () {
		DontDestroyOnLoad (gameObject);
		
		StartCoroutine(LoadMainLevelinBackground());
		
		//
		mainCube = transform.FindChild("Cube").transform;
	}

	public void EnterPlayMode() {
		foreach (PieceSnapping snapper in GetComponentsInChildren<PieceSnapping>()) {
			snapper.PrepareForGameplay();
		}
		//Application.LoadLevel (1);
		//transform.position = new Vector3 (40, 5, -5);
		
		
		LoadMainLevel();
	}
	
	IEnumerator LoadMainLevelinBackground(){
	
		AsyncOperation loadMainLevelOperation = Application.LoadLevelAsync("MainLevel");
		loadMainLevelOperation.allowSceneActivation = false;
		while(!loadMainLevel){
			yield return null;
		}
		loadMainLevelOperation.allowSceneActivation = true;
		yield return loadMainLevelOperation; 
	}
	
	void LoadMainLevel(){ //HACK TROLOLOLO
	
		var gateToOblivion = GameObject.FindGameObjectWithTag("MagicalObject");
		
		DontDestroyOnLoad(gateToOblivion);
	
		//OMG Don't look at this #HACK 
		// I wan't the player to be at start point on init
		// we should have system which does that
		//don't touch the camera!
		gateToOblivion.transform.position = new Vector3(-136f,9.5f,-122.8f);
		var newRot = new Quaternion();
		newRot.eulerAngles = new Vector3(358f,268f,1.49f);
		gateToOblivion.transform.rotation = newRot;
		//
		
		//set up camera
		Camera.main.GetComponent<CameraPivotRotation>().enabled = false;
	//	Camera.main.transform.parent = null;
		var camControl = Camera.main.gameObject.AddComponent<UltimateOrbitCamera>();
		
		var player = gateToOblivion.transform.FindChild("ParentusSupremus").GetComponent<ParentController>().mainCube.transform;
		
		camControl.target= player;
		
		camControl.transform.position =  new Vector3(-144f, 9f, 0f);
		newRot.eulerAngles = new Vector3(20f,83f,0f);
		camControl.transform.rotation = newRot;
		
		DontDestroyOnLoad(gateToOblivion.transform.FindChild("ParentusSupremus").gameObject);
		
		gateToOblivion.transform.FindChild("ParentusSupremus").transform.parent = null;
		
		
		DontDestroyOnLoad(Camera.main.gameObject);
		//
		loadMainLevel = true;

	}
	
	// Update is called once per frame
	void Update () {

		
		//ZERO< ZERO FUCKS GIVEN #HACK 2000
		Vector3 rotMod = new Vector3();
		if (Input.GetKey (KeyCode.A)) {
			//Debug.Log("SteeringThingy");
			
			transform.RotateAround(mainCube.transform.position, new Vector3(0,1f,0),-.75f);
							
		} else if (Input.GetKey (KeyCode.D)) {
			
			rotMod = new Vector3(0,-.75f,0f);
			transform.RotateAround(mainCube.transform.position, new Vector3(0,1f,0),.75f);
		}
		
		if (Input.GetKey (KeyCode.LeftControl) && Input.GetKey(KeyCode.R)) {
		
			Application.LoadLevel("mainMenu");
		}
		
	
		
	}

	void Save() {

	}
}
