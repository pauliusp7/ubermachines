﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof( AudioSource ))]
public class AudioPlay : MonoBehaviour {
	
	AudioSource source;
	public AudioClip shootingsound;
	public AudioClip explosionsound;
	public AudioClip hitsound;
	//and so on

	private AudioSource sound;
	
	void Start () 
	{
		source = GetComponent<AudioSource>();

		GameObject audioPlayObject2 = GameObject.FindWithTag ("AudioSource");
		if (audioPlayObject2 != null)
		{
			sound = audioPlayObject2.GetComponent <AudioSource>();
		}
		if (sound == null)
		{
			Debug.Log ("Cannot find 'AudioPlay' script");
		}
	}

	
	public void PlayWhenExplode()
	{
		if(sound.mute == false)
		{
			source.PlayOneShot( explosionsound, 0.5f );
		}
	}

	public void PlayOnHit()
	{
		if(sound.mute == false)
		{
		source.PlayOneShot( hitsound, 0.5f );
		}
	}

	public void PlayOnShot()
	{
		if(sound.mute == false)
		{
		source.PlayOneShot( shootingsound, 0.7f );
		}
	}


	
	

}
