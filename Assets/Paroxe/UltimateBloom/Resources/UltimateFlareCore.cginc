﻿
#include "UnityCG.cginc"

struct v2f 
{
	half4 pos : SV_POSITION;
	half2 uv : TEXCOORD0;
	half4 uv0 : TEXCOORD1;
	half4 uv1 : TEXCOORD2;
	half4 uv2 : TEXCOORD3;
	half4 uv3 : TEXCOORD4;
};



sampler2D _MainTex;
sampler2D _FlareTexture;
half _Intensity;

half4 _FlareScales;
half4 _FlareTint0;
half4 _FlareTint1;
half4 _FlareTint2;
half4 _FlareTint3;

half2 cUV(half2 uv)
{
	return 2.0 * uv - float2(1.0,1.0);
}

half2 tUV(half2 uv)
{
	return (uv + float2(1.0,1.0))*0.5;
}

v2f vert( appdata_img v ) 
{
	v2f o;
	o.pos = mul(UNITY_MATRIX_MVP, v.vertex);
	o.uv =  v.texcoord.xy;

	half scale0 = _FlareScales.x;//1.1f;
	half scale1 =  _FlareScales.y;//0.95f;
	half scale2 =  _FlareScales.z;//0.75f;
	half scale3 =  _FlareScales.w;//0.55f;

	half2 flareUv = cUV(half2(1.0,1.0) - o.uv);
	o.uv0.xy = tUV(flareUv*scale0);
	o.uv1.xy = tUV(flareUv*scale1); 
	o.uv2.xy = tUV(flareUv*scale2);
	o.uv3.xy = tUV(flareUv*scale3);

	flareUv = cUV(o.uv);
	o.uv0.zw = tUV(flareUv*scale0*2);
	o.uv1.zw = tUV(flareUv*scale1*2);
	o.uv2.zw = tUV(flareUv*scale2*2);
	o.uv3.zw = tUV(flareUv*scale3*2);

	return o;
} 

fixed4 frag(v2f i):COLOR
{
	half scale0 = _FlareScales.x;//1.1f;
	half scale1 =  _FlareScales.y;//0.95f;
	half scale2 =  _FlareScales.z;//0.75f;
	half scale3 =  _FlareScales.w;//0.55f;

	half2 flareUv = cUV(float2(1.0,1.0) - i.uv);

	float4 acc = float4(0,0,0,0);

	acc += tex2D(_MainTex, i.uv0.xy ) * _FlareTint0;
	acc += tex2D(_MainTex, i.uv1.xy ) * _FlareTint1;
	acc += tex2D(_MainTex, i.uv2.xy ) * _FlareTint2;
	acc += tex2D(_MainTex, i.uv3.xy ) * _FlareTint3;

#ifdef FLARE_DOUBLE
	flareUv = cUV(i.uv);
	acc += tex2D(_MainTex, i.uv0.zw ) * _FlareTint0;
	acc += tex2D(_MainTex, i.uv1.zw  ) * _FlareTint1;
	acc += tex2D(_MainTex, i.uv2.zw ) * _FlareTint2; 
	acc += tex2D(_MainTex, i.uv3.zw ) * _FlareTint3;
#endif

	/*
	acc += tex2D(_MainTex, tUV(flareUv*scale0) ) * _FlareTint0;
	acc += tex2D(_MainTex, tUV(flareUv*scale1) ) * _FlareTint1;
	acc += tex2D(_MainTex, tUV(flareUv*scale2) ) * _FlareTint2;
	acc += tex2D(_MainTex, tUV(flareUv*scale3) ) * _FlareTint3;

#ifdef FLARE_DOUBLE
	flareUv = cUV(i.uv);
	acc += tex2D(_MainTex, tUV(flareUv*scale0 * 2) ) * _FlareTint0;
	acc += tex2D(_MainTex, tUV(flareUv*scale1  * 2) ) * _FlareTint1;
	acc += tex2D(_MainTex, tUV(flareUv*scale2  * 2) ) * _FlareTint2; 
	acc += tex2D(_MainTex, tUV(flareUv*scale3 * 2) ) * _FlareTint3;
#endif
	*/
	return clamp(acc *_Intensity,0, 65000);
}


